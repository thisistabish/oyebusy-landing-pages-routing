/* eslint-disable react/prop-types */
/* eslint-disable no-unused-vars */
import React from 'react'
import CountUp from 'react-countup';

const Counter = ({value}) => {
  return (
    <CountUp end={value} />
  )
}

export default Counter