/* eslint-disable no-unused-vars */
/* eslint-disable react/prop-types */
import React, { useState, useEffect } from "react";

const Carousel = ({ data }) => {
  const [currentIndex, setCurrentIndex] = useState(0);

  useEffect(() => {
    const interval = setInterval(() => {
      setCurrentIndex((prevIndex) =>
        prevIndex === data.length - 1 ? 0 : prevIndex + 1
      );
    }, 4000);

    return () => {
      clearInterval(interval);
    };
  }, [data]);
  return (
    <div className="relative bg-white md:h-[120px] h-[200px] flex flex-col justify-center px-10 rounded-md">
      {data?.map((slide, index) => (
        <div
          key={index}
          className={`absolute ${
            index === currentIndex ? "opacity-100" : "opacity-0"
          } transition-opacity`}
        >
          <div className="">
            <h1 className="text-xl font-semibold">{slide?.title}</h1>
            <p className="pt-4">{slide?.description}</p>
          </div>
        </div>
      ))}
    </div>
  );
};

export default Carousel;
