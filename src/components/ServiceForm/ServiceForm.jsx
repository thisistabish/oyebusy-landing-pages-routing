/* eslint-disable react/prop-types */
/* eslint-disable no-unused-vars */
import React from "react";

const ServiceForm = ({ name }) => {
  return (
    <div>
      <form className="bg-white p-2 rounded-2xl">
        <p className="text-center text-lg py-1">Service Request From</p>
        <select
          className="w-full py-2 px-2 my-2 outline-none border border-[#d6eaff] rounded-md"
          required
        >
          <option value={name}>{name}</option>
        </select>
        <br />
        <input
          className="w-full py-2 px-2 my-2 outline-none border border-[#d6eaff] rounded-md"
          type="text"
          name="name"
          placeholder="Enter Name"
          required
        />
        <br />
        <input
          className="w-full py-2 px-2 my-2 outline-none border border-[#d6eaff] rounded-md"
          type="text"
          name="email"
          placeholder="Enter Address"
          required
        />
        <br />
        <input
          className="w-full py-2 px-2 my-2 outline-none border border-[#d6eaff] rounded-md"
          type="text"
          name="mobile"
          placeholder="Enter Mobile Number"
          required
        />
        <br />
        {/* <input
          className="w-full py-2 px-2 my-2 outline-none border border-[#d6eaff] rounded-md"
          type="date"
          name="date"
          defaultValue={todayDate}
          required
        /> */}
        <button
          type="submit"
          className="bg-[#FF9B4F] w-full py-2 uppercase text-xl  rounded-full text-white"
        >
          Submit
        </button>
        <p className="italic text-center text-black  text-lg py-8">
          100% privacy Guaranteed.
        </p>
      </form>
    </div>
  );
};

export default ServiceForm;
