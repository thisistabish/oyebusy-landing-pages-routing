/* eslint-disable no-unused-vars */
/* eslint-disable react/prop-types */
import React from "react";
import Carousel from "../../../components/Carousel/Carousel";

const Reason = ({ route }) => {
  return (
    <>
      <div className="bg-[#F2F2F2] py-12">
        <div className="max-w-screen-xl mx-auto px-3">
          <div className="pb-16">
            <p className="text-center text-lg">
              - {route?.pageData?.reasonToLove?.title} -
            </p>
            <h3 className="text-3xl text-center py-3">
              {route?.pageData?.reasonToLove?.subTitle}
            </h3>
            <p className="text-center md:px-56">
              {route?.pageData?.reasonToLove?.description}
            </p>
          </div>
          <Carousel data={route?.pageData?.reasonToLove?.features} />
        </div>
      </div>
      <div className="bg-[#FF9B4F] py-8 text-white text-center ">
        <h4 className="text-xl">{route?.pageData?.banner3?.title}</h4>
        <p>{route?.pageData?.banner3?.subTitle}</p>
      </div>
    </>
  );
};

export default Reason;
