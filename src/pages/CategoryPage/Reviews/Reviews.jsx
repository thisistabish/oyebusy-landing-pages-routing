/* eslint-disable react/prop-types */
/* eslint-disable no-unused-vars */
import React from "react";
import Carousel from "../../../components/Carousel/Carousel";

const Reviews = ({ route }) => {
  return (
    <div className="py-20 bg-[#F2F2F2]">
      <div className="max-w-screen-xl mx-auto px-3">
        <div className="pb-5">
          <p className="text-center text-lg">
            - {route?.pageData?.reviews?.title} -
          </p>
          <h3 className="md:text-3xl text-xl text-center py-3 md:px-28">
            {route?.pageData?.reviews?.subTitle}
          </h3>
        </div>
        <Carousel data={route?.pageData?.reviews?.features} />
      </div>
    </div>
  );
};

export default Reviews;
