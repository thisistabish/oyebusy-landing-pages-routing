import React from 'react'
import './Navbar.css'

import { Link } from 'react-router-dom'

const Navbar = ({ navData }) => {
    console.log(navData);
    return (
        <>
            <div className=' bg-white  '>
                <div className='max-w-screen-xl  pt-3 pb-3 flex justify-between m-auto'>
                    <img src={navData?.header?.logo} alt="" className='w-24' />
                    <div className='flex  '>
                        <ul className='list-none flex'>
                            <li className='pr-3'>
                                <img src={navData?.header?.phone?.icon} alt="" className='w-12 ' />

                            </li>
                            <li className='my-auto mx-0 pr-3 text-lg'>9990001089</li>

                            {navData?.header?.social?.map((image, index) => (

                                <Link to={image.path} key={index} className='my-auto mx-0 w-8 px-1'>
                                    <img src={image.icon} alt="" className='align-middle' />

                                </Link>

                            )

                            )}


                        </ul>
                    </div>
                </div>

            </div>
            {/* Menu */}
            <div className='w-full bg-[#ff9d00] p-2 '>
                <div className='max-w-screen-xl m-auto'>

                    {navData?.header?.menu.map((menuItem, index) => (
                        <div key={index} className='dropdown' >
                            <span className='dropbtn'>{menuItem?.handyMan?.displayName}</span>

                            <div className='dropdown_content'>
                                {menuItem?.handyMan?.dropDown.map((subMenuItem, subIndex) => (

                                    <Link key={subIndex} to={subMenuItem?.path}>{subMenuItem?.anchorName}</Link>

                                ))}

                            </div>
                            
                            <Link to={menuItem?.homeCleaning?.path}><span className='text-white'>{menuItem?.homeCleaning?.anchorName}</span></Link>
                            
                           
                            <span className='text-white' >{menuItem?.blog?.anchorName}</span>
                            <span className='text-white'>{menuItem?.contact?.anchorName}</span>


                        </div>
                    ))}

                </div>

            </div>


        </>
    )
}

export default Navbar