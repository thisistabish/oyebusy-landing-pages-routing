/* eslint-disable no-unused-vars */
/* eslint-disable react/prop-types */
import React from 'react';
import { FaLongArrowAltRight } from 'react-icons/fa';
import { Link } from 'react-router-dom';
import './BottomAbout.css'
import Counter from '../../../components/Counter/Counter';

const BottomAbout = ({route}) => {
    return (
        <div>
            <div className="py-10">
            <div className="max-w-screen-xl mx-auto px-3">
              <div>
                {route?.pageData?.about.map((item, index) => (
                  <div key={index}>
                    {index % 2 === 0 ? (
                      <div className="md:flex gap-10 mb-5 md:mb-10">
                        <div className="py-5 md:w-1/2">
                          <div
                            dangerouslySetInnerHTML={{
                              __html: item?.html.replace(
                                /<h4/g,
                                '<h4 class="' + ["custom-h4"] + '"'
                              ),
                            }}
                          />
                          <Link to={'#about'}>
                          <button className="bg-[#ff9b4f] flex items-center gap-2 py-3 px-5 rounded-lg text-white mt-5">
                            <FaLongArrowAltRight />
                            Learn More
                          </button>
                          </Link>
                        </div>
                        <div className="md:w-1/2">
                        <img src={item.img}  alt="" />
                        </div>
                      </div>
                    ) : (
                      <div className="flex flex-col-reverse md:flex-row gap-10 mb-5 md:mb-10">
                        <div className="md:w-1/2">
                        <img src={item.img}  alt="" />
                        </div>
                        <div className="md:w-1/2">
                        <div className="py-5">
                          <div
                            dangerouslySetInnerHTML={{
                              __html: item?.html.replace(
                                /<h4/g,
                                '<h4 class="' + ["custom-h4"] + '"'
                              ),
                            }}
                          />
                          <Link href={'#about'}>
                          <button className="bg-[#ff9b4f] flex items-center gap-2 py-3 px-5 rounded-lg text-white mt-5">
                            <FaLongArrowAltRight />
                            Learn More
                          </button>
                          </Link>
                        </div>
                        </div>
                      </div>
                    )}
                  </div>
                ))}
              </div>
            </div>
          </div>
          <div className="bg-[#ff9b4f] py-5">
            <div className="max-w-screen-xl mx-auto px-3">
              <div className="md:flex justify-between">
                {route?.pageData?.banner4?.features.map((item, index) => (
                  <div key={index}>
                    <div className="flex flex-col items-center text-white py-5 md:py-0">
                      <h3 className="text-4xl font-bold">
                        <Counter value={item?.value} />
                      </h3>
                      <p>{item.text}</p>
                    </div>
                  </div>
                ))}
              </div>
            </div>
          </div>
        </div>
    );
};

export default BottomAbout;