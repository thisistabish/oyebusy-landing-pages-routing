/* eslint-disable no-unused-vars */
/* eslint-disable react/prop-types */
import React from "react";
import { FaLongArrowAltRight, FaRegCheckCircle } from "react-icons/fa";
import { Link } from "react-router-dom";

const AboutServices = ({ route }) => {
  return (
    <>
      <div className="max-w-screen-xl mx-auto px-3">
        <div>
          <p className="text-center pt-10 text-lg">
            - {route?.pageData?.aboutServices?.title} -
          </p>
          <h3 className="text-4xl text-center py-3">
            {route?.pageData?.aboutServices?.subTitle}
          </h3>

          <div className=" md:px-10">
            {route?.pageData?.aboutServices?.services.map((item, index) => (
              <div key={index} className="md:py-10">
                {index % 2 === 0 ? (
                  <div className="flex flex-col-reverse md:flex-row justify-between md:items-center">
                    <img
                      src={item?.img}
                      alt=""
                      className="mt-10 md:mt-0 md:w-[50%]"
                    />
                    <div>
                      <p className="text-2xl pb-5 pt-5 md:pt-0">{item.title}</p>
                      {item.features.map((feature, index) => (
                        <p className="flex items-center gap-2 pb-2" key={index}>
                          <FaRegCheckCircle
                            className="text-[#ff9b4f]"
                            size={25}
                          />
                          {feature}
                        </p>
                      ))}
                      <Link to={"#top"}>
                        <button className="bg-[#ff9b4f] flex items-center gap-2 py-3 px-5 rounded-lg text-white mt-5">
                          <FaLongArrowAltRight />
                          Book Now
                        </button>
                      </Link>
                    </div>
                  </div>
                ) : (
                  <div className="md:flex justify-between items-center">
                    <div>
                      <p className="text-2xl pb-5 pt-5 md:pt-0">{item.title}</p>
                      {item.features.map((feature, index) => (
                        <p className="flex items-center gap-2 pb-2" key={index}>
                          <FaRegCheckCircle
                            className="text-[#ff9b4f]"
                            size={25}
                          />
                          {feature}
                        </p>
                      ))}
                      <Link href={"#top"}>
                        <button className="bg-[#ff9b4f] flex items-center gap-2 py-3 px-5 rounded-lg text-white mt-5">
                          <FaLongArrowAltRight />
                          Book Now
                        </button>
                      </Link>
                    </div>
                    <img
                      src={item?.img}
                      alt=""
                      className="mt-10 md:mt-0 md:w-[50%]"
                    />
                  </div>
                )}
              </div>
            ))}
          </div>
        </div>
      </div>
      <div
              style={{
                backgroundImage: `linear-gradient(rgba(0, 0, 0, 0.3), rgba(0, 0, 0, 0.3)), url(${route?.pageData?.banner2?.bgImg})`,
                backgroundSize: "cover",
                backgroundPosition: "center",
                backgroundAttachment: "fixed",
                height: "120px",
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <div className="md:flex items-center justify-center gap-5 px-3">
                <h4 className="text-2xl text-white">
                  {route?.pageData?.banner2?.title}
                </h4>
                <Link href={'#about'}>
                <button className="bg-[#ff9b4f] flex items-center gap-2 py-3 px-5 rounded-lg text-white">
                  <FaLongArrowAltRight />
                  Learn More
                </button>
                </Link>
              </div>
            </div>
    </>
  );
};

export default AboutServices;
