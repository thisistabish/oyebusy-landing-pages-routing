/* eslint-disable react/prop-types */
/* eslint-disable no-unused-vars */
import React from "react";
import { Link } from "react-router-dom";
import { FaCalendarAlt, FaHome, FaLock } from "react-icons/fa";
import ServiceForm from "../../../components/ServiceForm/ServiceForm";

const Banner = ({ route }) => {
  return (
    <div>
      <div>
        <div
          style={{
            backgroundImage: `linear-gradient(rgba(0, 0, 0, 0.3), rgba(0, 0, 0, 0.3)), url(${route?.pageData?.heroSection?.bgImage})`,
          }}
          className="bg-plumber-hero-banner bg-no-repeat bg-cover bg-bottom bg-black"
        >
          <div className="relative w-full h-full">
            <div className="max-w-screen-xl mx-auto  md:px-20 px-3 py-20">
              <div className="text-white pb-8">
                <p>
                  <Link to={"/"}>
                    {
                      route?.pageData?.heroSection?.breadcrumbItems[0]
                        .displayName
                    }
                  </Link>
                  {" > "}
                  <span className="text-yellow-300">
                    {
                      route?.pageData?.heroSection?.breadcrumbItems[1]
                        .displayName
                    }
                  </span>
                </p>
              </div>
              <div className="flex flex-col-reverse md:flex-row justify-between gap-20">
                <div id="top" className="md:w-1/3">
                  <ServiceForm
                    name={
                      route?.pageData?.heroSection?.breadcrumbItems[1]
                        .displayName
                    }
                  />
                </div>
                <div className="md:w-2/3">
                  <div className="text-white">
                    <h3 className="text-3xl md:text-5xl pb-5">
                      {route?.pageData?.heroSection?.heading1}
                    </h3>
                    <div className="flex flex-col gap-5">
                      {route?.pageData?.heroSection?.heroBullets?.map(
                        (item, index) => (
                          <div key={index} className="flex items-center gap-5">
                            <div className="bg-slate-100 bg-opacity-25 p-3 md:p-5 rounded-full">
                              {(item.priority === 1 && (
                                <FaCalendarAlt size={35} />
                              )) ||
                                (item.priority === 2 && <FaLock size={35} />) ||
                                (item.priority === 3 && <FaHome size={35} />)}
                            </div>
                            <div>
                              <h5 className="text-xl">
                                {item.priority} . {item.title}
                              </h5>
                              <p className="md:text-lg">{item.description}</p>
                            </div>
                          </div>
                        )
                      )}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="bg-[#FF9B4F]">
          <h3 className="text-center text-white text-2xl md:text-3xl py-8  md:py-12">
            {route?.pageData?.banner1.title}
          </h3>
        </div>
      </div>
    </div>
  );
};

export default Banner;
