import React from 'react'
import { Link } from 'react-router-dom'

const Footer = ({ navData }) => {
    return (
        <div className='bg-[#00173e] p-20 w-full  '>
            <div className='max-w-screen-xl flex m-auto ' >
                <div className='w-1/4'>
                    <img src={navData?.footer?.logo} alt="" />
                    <p className='text-white text-xl pt-5'>{navData?.footer?.tagLine}</p>
                </div>

                <div className='w-3/4'>
                    <p className='text-white pb-5 text-xl' >{navData?.footer?.quickLink?.heading}</p>
                    <div  >{navData?.footer?.quickLink?.cities?.map((citiesData, index) => (
                        <Link to={citiesData?.path} key={index} className='text-white mt-10 pl-6'>
                            {citiesData?.anchorName}
                        </Link>

                    ))}
                    </div>

                </div>
            </div></div>


    )
}

export default Footer