/* eslint-disable react/prop-types */
/* eslint-disable no-unused-vars */
import React from "react";
import "./About.css";

const About = ({ route }) => {
  return (
    <div className="max-w-screen-xl mx-auto px-3">
      <div>
        <p className="text-center py-10 text-lg">
          - {route?.pageData?.mainContent.title} -
        </p>
        <div
          dangerouslySetInnerHTML={{
            __html: route?.pageData?.mainContent?.html
              .replace(/<h3/g, '<h3 class="' + ["custom-h3"] + '"')
              .replace(/<ul/g, '<ul class="' + ["custom-list"] + '"')
              .replace(/<p/g, '<p class="' + ["custom-p"] + '"'),
          }}
        ></div>
      </div>
    </div>
  );
};

export default About;
