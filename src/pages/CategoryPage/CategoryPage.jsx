/* eslint-disable no-unused-vars */
import React, { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router";
import Banner from "./Banner/Banner";
import About from "./About/About";
import AboutServices from "./AboutServices/AboutServices";
import Reason from "./Reason/Reason";
import Reviews from "./Reviews/Reviews";
import BottomAbout from "./BottomAbout/BottomAbout";
import NavBar from "./NavBar/Navbar";
import Footer from "./Footer/Footer";

const CategoryPage = () => {
  const params = useParams();
  const navigate = useNavigate()
  const path = "/" + params.path + "/";
  console.log(path);
  const [route, setRoute] = useState([]);
  const [navData, setNavData] = useState([]);

  useEffect(() => {
    fetch("/routes.json")
      .then((data) => data.json())
      .then((data) => setRoute(data));

  }, []);
  useEffect(() => {
    fetch("/navfooter.json")
      .then((data) => data.json())
      .then((data) => setNavData(data));

  }, []);

  const matchRoute = route.find((data) => data.path === path);
  console.log(matchRoute);
  console.log(navData);


  if (!matchRoute) {
    navigate("/404");
    return null;
  }

  return (
    <div>
      <NavBar navData={navData} />
      <Banner route={matchRoute} />
      <About route={matchRoute} />
      <AboutServices route={matchRoute} />
      <Reason route={matchRoute} />
      <Reviews route={matchRoute} />
      <BottomAbout route={matchRoute} />
      <Footer navData={navData} />
    </div>
  );
};

export default CategoryPage;
