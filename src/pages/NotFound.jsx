/* eslint-disable no-unused-vars */
import React from 'react';
import { Link } from 'react-router-dom';

const NotFound = () => {
    return (
        <div className='h-screen w-full flex justify-center items-center bg-[#1A2238]'>
            <div className='text-white'>
                <h1 className='text-9xl font-bold text-white'>404</h1>
                <p className='text-center'>Opps. THis page has gone missing</p>
                <Link to={'/'}>
                <button className='border-2 border-white px-20 mt-5 hover:bg-white hover:text-black'>Go Home</button>
                </Link>
            </div>
        </div>
    );
};

export default NotFound;