import { useState } from 'react';

import { useNavigate } from 'react-router-dom';

const Home = () => {
  const history = useNavigate();

  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  const handleLogin = () => {
    // Replace these with your actual login logic
    const correctUsername = 'Tabish';
    const correctPassword = '12345';

    if (username === correctUsername && password === correctPassword) {
      // Redirect to /plumbers/
      history('/plumbers/');
    } else {
      // Display error message
      alert('Login details incorrect');
    }
  };
  return (
    <div className="min-h-screen flex items-center justify-center bg-gray-50">
      <div className="max-w-md w-full bg-white p-6 rounded-lg shadow-md">
        <h1 className="text-3xl font-bold text-center mb-8">OyeBusy Home Service</h1>
        <h2 className="text-2xl text-center mb-4">Login Page</h2>
        <input
          type="text"
          placeholder="Username"
          className="w-full px-4 py-2 mb-4 border rounded-md focus:outline-none focus:border-blue-500"
          value={username}
          onChange={(e) => setUsername(e.target.value)}
        />
        <input
          type="password"
          placeholder="Password"
          className="w-full px-4 py-2 mb-4 border rounded-md focus:outline-none focus:border-blue-500"
          value={password}
          onChange={(e) => setPassword(e.target.value)}
        />
        <button
          onClick={handleLogin}
          className="w-full px-4 py-2 text-white bg-blue-500 rounded-md hover:bg-blue-600 focus:outline-none"
        >
          Login
        </button>
      </div>
    </div>
  )
}

export default Home
