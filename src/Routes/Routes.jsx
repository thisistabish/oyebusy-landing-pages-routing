import { createBrowserRouter } from "react-router-dom";
import Home from "../pages/Home/Home";
import CategoryPage from "../pages/CategoryPage/CategoryPage";
import NotFound from "../pages/NotFound";

const router = createBrowserRouter([
  {
    path: "/",
    element: <Home />,
  },
  {
    path: '/404',
    element: <NotFound />
  },
  {
    path: '/:path',
    element: <CategoryPage />
  }
]);

export default router;
